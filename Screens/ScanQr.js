import {Box, Button, Center, IconButton, Text} from 'native-base';
import {useState} from 'react';
import QRCodeScanner from 'react-native-qrcode-scanner';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {Client, Databases, ID, Query} from 'appwrite';
import {
  APPWRITE_PROJECT_ID,
  APPWRITE_DATABASE_ID,
  APPWRITE_ATTENDANCE_COLLECTION_ID,
  APPWRITE_USERS_COLLECTION_ID,
} from '@env';
import {Alert} from 'react-native';

const ScanQr = ({navigation}) => {
  const [data, setData] = useState('');

  const removeItem = async () => {
    await AsyncStorage.removeItem('UID')
      .then(() => {
        navigation.replace('Login');
      })
      .catch(error => console.log(error));
  };
  navigation.setOptions({
    headerRight: () => {
      return (
        <IconButton
          onPress={() => {
            removeItem();
          }}
          icon={
            <MaterialIcons name="logout" size={24} color="black" />
          }></IconButton>
      );
    },
  });

  const handleRead = async e => {
    const client = new Client();
    const databases = new Databases(client);
    client.setEndpoint('https://cloud.appwrite.io/v1');
    client.setProject(APPWRITE_PROJECT_ID);
    await AsyncStorage.getItem('UID')
      .then(uid => {
        // const q = query(collection(db, 'users'), where('uid', '==', uid));
        // onSnapshot(q, querySnapshot => {
        //   querySnapshot.forEach(doc => {
        //     addDoc(collection(db, 'attendance'), {
        //       name: doc.data().name,
        //       email: doc.data().email,
        //       enrollmentno: doc.data().enrollmentno,
        //       date: new Date().toUTCString(),
        //       time: new Date().toLocaleTimeString(),
        //       collegeName: e.data.toUpperCase(),
        //     });
        //   });
        // });
        const promise = databases.listDocuments(
          APPWRITE_DATABASE_ID,
          APPWRITE_USERS_COLLECTION_ID,
          [Query.equal('uid', [uid])],
        );
        promise
          .then(function (response) {
            const promise1 = databases.createDocument(
              APPWRITE_DATABASE_ID,
              APPWRITE_ATTENDANCE_COLLECTION_ID,
              ID.unique(),
              {
                name: response.documents[0].name,
                email: response.documents[0].email,
                enrollmentno: response.documents[0].enrollmentno,
                date: new Date().toUTCString(),
                time: new Date().toLocaleTimeString(),
                collegeName: e.data.toUpperCase(),
              },
            );
            promise1
              .then(function (response) {})
              .catch(function (error) {
                Alert.alert('Error', error.message);
              });
          })
          .catch(function (error) {
            Alert.alert('Error', error.message);
          });
      })
      .catch(error => {
        console.log(error);
      });
    setData(e.data);
    navigation.navigate('Success');
  };
  return (
    <>
      {data ? (
        <>
          <Box
            height={'100%'}
            display={'flex'}
            flexDirection={'column'}
            alignItems={'center'}
            justifyContent={'center'}>
            <Button
              onPress={() => {
                setData('');
              }}>
              <Text
                style={{
                  padding: 2,
                  color: 'white',
                  fontSize: 18,
                }}>
                Scan Again
              </Text>
            </Button>
          </Box>
        </>
      ) : (
        <Box height={'100%'}>
          <QRCodeScanner
            onRead={e => handleRead(e)}
            reactivate={data ? false : true}
            showMarker={true}></QRCodeScanner>
        </Box>
      )}
    </>
  );
};

export default ScanQr;
